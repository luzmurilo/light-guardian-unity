using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    //public static ObjectPool SharedInstance;
    public List<GameObject> pooledObjects;
    public List<GameObject> objectsToPool;
    public int amountToPool;
    private int nextIndex;

    // private void Awake() 
    // {
    //     if(ObjectPool.SharedInstance == null)
    //     {
    //         SharedInstance = this;
    //     }
    //     else
    //     {
    //         Destroy(gameObject);
    //         return;
    //     }
    // }

    private void Start() 
    {
        pooledObjects = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < amountToPool; i++)
        {
            tmp = Instantiate(objectsToPool[Random.Range(0, objectsToPool.Count)], transform);
            tmp.SetActive(false);
            pooledObjects.Add(tmp);
        }
        nextIndex = 0;
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < amountToPool; i++)
        {
            if (nextIndex >= amountToPool)
            {
                nextIndex = 0;
            }
            if (!pooledObjects[nextIndex].activeInHierarchy)
            {
                //nextIndex++;
                return pooledObjects[nextIndex];
            }
            nextIndex++;
        }
        Debug.LogWarning("Unable to get pooled object");
        return null;
    }

    public void DeactivateObjects()
    {
        for (int i = 0; i < amountToPool; i++)
        {
            if (pooledObjects[i].activeInHierarchy)
            {
                pooledObjects[i].SetActive(false);
            }
        }
    }
}
