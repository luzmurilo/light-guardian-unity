using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpawnTiles : MonoBehaviour
{
    public static SpawnTiles SharedInstance;

    private GameObject[][] tiles;
    [SerializeField] GameObject tilePrefab;
    [SerializeField] TextMeshProUGUI priceText;
    [SerializeField] Vector3 priceTextOffset;
    public Vector2Int gridSize;
    public float tileSpacing = 0;
    public int energyPrice = 0;

    private AudioSource audioSource;
    [SerializeField] AudioClip obstacleActivateSound;

    private void Awake() 
    {
        if(SpawnTiles.SharedInstance == null)
        {
            SharedInstance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        switch(MainManager.SharedInstance.difficulty)
        {
            case 0:
                gridSize = new Vector2Int(4, 3);
            break;
            case 1:
                gridSize = new Vector2Int(6, 4);
            break;
            case 2:
                gridSize = new Vector2Int(8, 4);
            break;
            default:
                gridSize = new Vector2Int(4, 4);
            break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();

        transform.position = new Vector3((3 - (gridSize.x / 2)) * 3, 0.1f, 0);

        if (priceText == null)
        {
            priceText = GameObject.Find("Price Text").GetComponent<TextMeshProUGUI>();
        }
        priceText.text = "";

        SpawnNewTileGrid(gridSize.x, gridSize.y);
    }

    private void Update() 
    {
        priceText.transform.position = Input.mousePosition + priceTextOffset;
    }

    private void SpawnNewTileGrid(int x, int y)
    {
        tiles = new GameObject[x][];
        for (int i = 0; i < x; i++)
        {
            tiles[i] = new GameObject[y];
            for (int j = 0; j < y; j++)
            {
                Vector3 tilePosition = transform.position + (Vector3.right * (3 + tileSpacing) * i) + (Vector3.forward * (3 + tileSpacing) * j);
                tiles[i][j] = Instantiate<GameObject>(tilePrefab, tilePosition, transform.rotation, transform);
                tiles[i][j].GetComponent<TileManager>().Index = new Vector2Int(i, j);
            }
        }
        //tiles[3][3].transform.GetChild(0).gameObject.SetActive(true);
    }

    public void SetObstaclesActive(Vector2Int index)
    {
        if (energyPrice <= GameManager.SharedInstance.energy)
        {
            for(int i = index.y; i >= 0; i--)
            {
                if (!tiles[index.x][i].GetComponent<TileManager>().obstacle.activeInHierarchy)
                {
                    tiles[index.x][i].GetComponent<TileManager>().obstacle.SetActive(true);
                }
            }
            GameManager.SharedInstance.UpdateEnergyText(-energyPrice);
            audioSource.PlayOneShot(obstacleActivateSound);
        }
    }

    public void HighlightTiles(Vector2Int index, Color highlightColor, bool visible)
    {
        energyPrice = 0;
        int energyDiscount = 0;
        for(int i = index.y; i >= 0; i--)
        {
            tiles[index.x][i].GetComponent<Renderer>().material.color = highlightColor;
            tiles[index.x][i].GetComponent<Renderer>().enabled = visible;
            if (!tiles[index.x][i].transform.GetChild(0).gameObject.activeInHierarchy && visible)
            {
                energyPrice += 10 - energyDiscount;
                energyDiscount++;
            }
        }
        if (visible)
        {
            priceText.text = ("-" + energyPrice);
        }
        else
        {
            priceText.text = "";
        }
    }
}
