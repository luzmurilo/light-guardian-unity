using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 1.0f;
    public int energyReward = 8;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Obstacle"))
        {
            HandleObstacleHit(other); // ABSTRACTION
        }
        else if (other.CompareTag("Base"))
        {
            Debug.Log("Game Over!");
            GameManager.SharedInstance.SetGameOverState(true);
            speed = 0;
            //Destroy(gameObject);
        }
    }

    protected virtual void HandleObstacleHit(Collider obstacle) // ABSTRACTION
    {
            obstacle.transform.GetComponentInParent<TileManager>().DestroyObstacle(energyReward);

            if (!GameManager.SharedInstance.isUsingTimer)
            {
                GameManager.SharedInstance.UpdateCountDownText(-1);
            }
            
            gameObject.SetActive(false);
    }
}
